Please read the information/instruction before starting to use this component. <br>
OBJECTIVE:<br>
The objective of this implementation is to enable easy usage of camel's elasticsearch component within talend, and still harnessing the power of different language options that is provided by talend. <br>
DEVELOPER NOTE:<br>
This component uses the functionality of talend(camel) header component and camel's Processor. In a way it's a component based on logic aggregation. <br>
USAGE:<br
- The component was designed/developed with talend 6.3.1 which uses Camel 2.17.3 version. Going by the documentation, it's compactable with 2.2 version of elasticsearch however, I have used v2.0 elasticsearch for testing. The rest of the instructions would be based on v2.0 of elasticsearch. <br


- Download the folder ElasticLog and place it in ${talend_home_dir}\plugins\org.talend.designer.camel.components.localprovider_6.3.1.20161216_1026\components\ <br

PREPARING ELASTICSEARCH <br>
Download elasticsearch from here. <br>
https://www.elastic.co/downloads/past-releases/elasticsearch-2-0-0 <br>
After starting the elasticsearch, execute the following rest query to create the index, and defined index_type. <br>

curl -XPOST localhost:9200/cimt_nl_index -d '{
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0
    },
    "_mapping": {
        "talend_logging_type": {
            "timestamp": {
      "enabled": true,
      "type": "date",
      "format": "yyyy-MM-dd HH:mm:ss SSS",
      "store": true
    }
        }
    }
}'



The default elastic's transport client is 9300. <br>

TALEND ROUTE CONFIGURATION: <br>
1. A simple route is placed as described here and the message in my body contains "Hello Elastic World".  <br>
https://snag.gy/rxRmkf.jpg <br>

2. In cConfig camel-elasticsearch-alldep-2.17.3.jar is added to support elastic. <br>
https://snag.gy/D7f20Z.jpg <br>

3. The configuration of elastic component here.   https://snag.gy/BZpNGf.jpg <br>

4. The output based on above configurations would look like here. https://snag.gy/CKuiy9.jpg <br>





